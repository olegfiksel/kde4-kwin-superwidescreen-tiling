# Tile windows on a widescreen using hotkeys

![](/images/image01.png)

## Install

```
git clone <this repo>
cd kde4-kwin-superwidescreen-tiling
plasmapkg --install .
```

## Uninstall

```
plasmapkg --remove .
```

## Activate

* Start **System Settings** (`systemsettings`)
* Navigate to **Window Behavior**
* Select **KWin Scripts** on the left side
* Tick the box on **Superwidescreen tilling**

# Ressources

* https://github.com/piehei/kde-plasma-center-window
* https://techbase.kde.org/Development/Tutorials/KWin/Scripting