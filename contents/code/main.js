function tileWindow(part, taskbarWidth, debug) {
  var win = workspace.activeClient;
  var height = workspace.displayHeight;
  var width = workspace.displayWidth;

  var winPosX = Math.round(0+(((part-1)/3)*width));
  var winPoxY = 0;
  var winHeight = height;
  var winWidth = Math.floor(width / 3);
  if(part==2){
    winHeight = height-taskbarWidth;
  }

  if(debug){
    print("part: "+part);
    print("taskbarWidth: "+taskbarWidth);
    print("winPosX: "+winPosX);
    print("winPoxY: "+winPoxY);
    print("winHeight: "+winHeight);
    print("winWidth: "+winWidth);
  }

  if(win.resizeable){
    win.geometry = {
        x: winPosX,
        y: winPoxY,
        height: winHeight,
        width: winWidth,
    }
  }
}

registerShortcut("Move and resize the current window to 1/3 of screen", "Move and resize the current window to 1/3 of screen", "Alt+9", function() {
  tileWindow(1, 36, 0);
});
registerShortcut("Move and resize the current window to 2/3 of screen", "Move and resize the current window to 2/3 of screen", "Alt+9", function() {
  tileWindow(2, 36, 0);
});
registerShortcut("Move and resize the current window to 3/3 of screen", "Move and resize the current window to 3/3 of screen", "Alt+9", function() {
  tileWindow(3, 36, 0);
});