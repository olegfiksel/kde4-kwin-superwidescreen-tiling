# v1.0.1

## Bugfixes

* Resize only resizible windows

## Documentation

* Add how to active the script

# v1.0.0

Initial release
